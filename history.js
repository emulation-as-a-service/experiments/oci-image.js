#!/usr/bin/env node

const fs = require("fs").promises;
const crypto = require("crypto");

class BlobStore {
    constructor(path) {
        this.path = path;
    }
    getPath(idOrObject) {
        const id = idOrObject.digest || idOrObject;
        if (id === "index.json") return `${this.path}/${id}`;
        const [algo, hash] = id.match(/^(\w+):(\w+)$/).slice(1);
        return `${this.path}/blobs/${algo}/${hash}`;
    }
    async get(id) {
        return fs.readFile(this.getPath(id));
    }
    async getJSON(id) {
        return JSON.parse((await this.get(id)).toString());
    }
    async getID(value) {
        return `sha256:${crypto.createHash("sha256").update(value).digest("hex")}`;
    }
    async write(value, id = this.getID(value)) {
        id = await id;
        await this._writeRaw(id, value);
        return {digest: id, size: value.length};
    }
    async _writeRaw(id, value) {
        return fs.writeFile(this.getPath(id), value);
    }
    async writeJSON(object, id) {
        return this.write(JSON.stringify(object), id);
    }
}

(async () => {
    const [ociImagePath] = process.argv.slice(2);

    const store = new BlobStore(ociImagePath);
    const index = await store.getJSON("index.json");
    const image = await store.getJSON(index.manifests[0]);
    const config = await store.getJSON(image.config);
    const history = config.history;

    /*
    image.layers = image.layers.slice(0, 1);
    index.manifests[0] = await store.writeJSON(image);
    */

    /*
    config.rootfs.diff_ids = config.rootfs.diff_ids.slice(0, 1);

    Object.assign(image.config, await store.writeJSON(config));
    Object.assign(index.manifests[0], await store.writeJSON(image));
    store.writeJSON(index, "index.json");
    */

    console.log(image, history);
})();
